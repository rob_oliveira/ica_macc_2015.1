rm(list = ls())
library(gdata)#data = read.csv("datasets/wine.csv",  header=FALSE)
library(scatterplot3d)
library(rgl)
attach(mtcars)
options("na.action")
source("functions.r")

#data$V2 <- strptime(data$V1,"%Y-%m-%d %H:%M:%S")
#print(data$V2)
data = read.table("teste_hora_2",  header=FALSE)
data = na.omit(data)
colnames(data) <- (c("id", "x", "y", "temperatura", "humidade", "luminosidade", "voltagem"))
sensor_data = data[!duplicated(data$id),]

num_coluna = ncol(sensor_data)
temperatura = sensor_data[,4]
humidade = sensor_data[,5]
luz = sensor_data[,7]
posicoes = sensor_data[, c(2,3)]


trainingLms <- function(valores_treinamento_, resultado_treinamento_) {

	S = as.matrix(valores_treinamento_)
	
	Xt = solve(t(S)%*%S)%*%t(S)

	w = Xt%*%resultado_treinamento_

	return(w)
}

plotDataSurface <- function(result, dados, color = "red", name="teste"){
	#dev.new(); h1=dev.cur()
	regression <- lm(dados[,3] ~ I(dados[,1]*result[1,1]) + I(dados[,2]*result[2,1]))
	
	x = dados$x
	y = dados$y
	f <- function(x,y) { r <- (x*result[1,1]) + (y*result[2,1]) ;}

	z <- outer(x, y, f)
		print(z)
	#im <- with(dados,interp(x,y,z))
	#with(im,image(x,y,z))
	zlim <- range(z)
	zlen <- zlim[2] - zlim[1] + 1
	colorlut <- heat.colors(zlen,alpha=0.3) # height color lookup table
	col <- colorlut[ z-zlim[1]+1 ]
	print(dim(z))
	plot3d(x, y, z)
	surface3d(x, y, z, col=col)
	#persp(x, y, z, col=color)
}

plotDataLinear <- function(result, dados, color = "red", name="teste"){
	dev.new(); h1=dev.cur()
	
	regression <- lm(dados[,3] ~ I(dados$x*result[1,1]) + I(dados$y*result[2,1]))
	s3d <- scatterplot3d(dados, main=name, pch = 16, highlight.3d=TRUE,
                              type="h", angle = 50)
	s3d$plane3d(regression, col=color)
}

plotDataLinearToFile <- function(result, dados, color = "red", name="teste"){
	filename = paste(c(name, ".png"), collapse="")
	print(filename)
	png(filename)
	regression <- lm(dados[,3] ~ I(dados[,1]*result[1,1]) + I(dados[,2]*result[2,1]))
	s3d <- scatterplot3d(dados, main=name, pch = 16, highlight.3d=TRUE,
                              type="h", angle = 50)
	s3d$plane3d(regression, col=color)
	dev.off()
}



temp_plot = sensor_data[, c(2:4)]
humi_plot = sensor_data[, c(2, 3, 5)]
luz_plot  = sensor_data[, c(2, 3, 6)]

erro = c(0,0,0)
for (i in 1:nrow(posicoes)) {
	res_temp = trainingLms(posicoes[-i,], temperatura[-i])
	res_hum = trainingLms(posicoes[-i,], humidade[-i])
	res_luz = trainingLms(posicoes[-i,], luz[-i])

	one_out_temp = posicoes[i, 1] * res_temp[1] + posicoes[i, 2] * res_temp[2]
	one_out_humi = posicoes[i, 1] * res_hum[1] + posicoes[i, 2] * res_hum[2]
	one_out_lumi = posicoes[i, 1] * res_luz[1] + posicoes[i, 2] * res_luz[2]
	
	erro[1] = erro[1] + (temperatura[i] - one_out_temp)^2
	erro[2] = erro[2] + (humidade[i] - one_out_humi)^2
	erro[3] = erro[3] + (luz[i] - one_out_lumi)^2
}
print(erro/nrow(posicoes))


#lotDataSurface(res_temp, temp_plot)
#plotDataLinear(res_temp, temp_plot, name="Temperatura")
#plotDataLinear(res_hum, humi_plot, color="blue", name="Humidade" )
#plotDataLinear(res_luz, luz_plot, color="purple", name="Luminosidade")
#plotDataLinearToFile(res_temp, temp_plot, name="images\\Temperatura_hora_1800")
#plotDataLinearToFile(res_hum, humi_plot, color="blue", name="images\\Humidade_hora_1800" )
#plotDataLinearToFile(res_luz, luz_plot, color="purple", name="images\\Luminosidade_hora_1800")



#write.csv(resultado_treino["pesos"], file="perceptronwinepeso.csv")
#write.csv(resultado_treino["confusao"], file="perceptronwineconf.csv")