rm(list = ls())
library(gdata)#data = read.csv("datasets/wine.csv",  header=FALSE)
library(scatterplot3d)
library(rsm)
library(rgl)

require(akima)
#attach(mtcars)

options("na.action")
source("functions.r")


data = read.table("teste_hora_4",  header=FALSE)
data = na.omit(data)
colnames(data) <- (c("id", "x", "y", "temperatura", "humidade", "luminosidade", "voltagem"))
sensor_data = data[!duplicated(data$id),]
num_coluna = ncol(sensor_data)
temperatura = sensor_data[,4]
humidade = sensor_data[,5]
luz = sensor_data[,7]
posicoes = sensor_data[, c(2,3)]
pesos = rep(0, nrow(sensor_data))

variancia_x = var(posicoes$x)
variancia_y = var(posicoes$y)

gaussian <- function(pontos, pesos){
	
	a = 1/sqrt(2*pi)
	n = nrow(pesos)
	print(pesos)
	xlim <- range(pontos$x)
	xlen <- seq(xlim[1], xlim[2] + 1)
	ylim <- range(pontos$y)
	ylen <- seq(ylim[1], ylim[2] + 1)
	Z = diag(0, nrow = xlim[2], ncol = ylim[2])

	for (i in 1:xlim[2]) {
		for (j in 1:ylim[2]) {
			for (k in 1:n) {
				g = a*exp((xlen[i] - pontos[k, 1])^2/(-2*variancia_x) + ( ylen[j] - pontos[k, 2])^2/(-2*variancia_y))
				
				Z[i, j] = Z[i,j] + pesos[k]*g
			}
		}
	}
	#print(Z/n)
	return(Z/n)

}

plotDataSurface <- function(result, dados, color = "red", name="teste"){
	z = gaussian(posicoes, result)

	#im <- with(dados,interp(x,y,z))
	#with(im,image(x,y,z))
	zlim <- range(z)
	zlen <- zlim[2] - zlim[1] + 1
	colorlut <- rainbow(zlen,alpha=0.5) # height color lookup table
	col <- colorlut[ z-zlim[1]+1 ]
		
	#plot3d(dados)
	#surface3d(1:nrow(z), 1:ncol(z), z, col=col, alpha=0.5, xlab="X", ylab="Y")
	persp3d(1:nrow(z), 1:ncol(z), z, col=col, alpha=0.5, xlab="X", ylab="Y", axes=TRUE)
}

gaussianKernel <- function(dados, resultado){
	pontos = as.matrix(dados)
	resultado = as.matrix(resultado)
	row = nrow(pontos)
	cov_mat = diag(0, nrow = row, ncol = row)

	a = 1 /sqrt(2*pi)
	for (i in 1:row) {
		for (j in 1:row) {
			distance =  (pontos[i, 1] - pontos[j, 1])^2/0.9*variancia_x + (pontos[i, 2] - pontos[j, 2])^2/0.9*variancia_y
			#print(resultado[i] - a*exp(distance/-2))
			cov_mat[i,j] = a*exp(distance/-2)
		}
	}

	
	inversa = solve(t(cov_mat)%*%cov_mat)
	pesos = inversa%*%t(cov_mat)%*%resultado
	#print(inversa)
	
	return(pesos)
}



#peso_temp = gaussianKernel(posicoes, temperatura)
#peso_humi = gaussianKernel(posicoes, humidade)
peso_lumi = gaussianKernel(posicoes, luz)

temp_plot = sensor_data[, c(2:4)]
humi_plot = sensor_data[, c(2, 3, 5)]
luz_plot  = sensor_data[, c(2, 3, 6)]

#print(retorno)
#plotDataSurface(retorno, temp_plot)
#plotDataSurface(peso_humi, humi_plot)
plotDataSurface(peso_lumi, luz_plot)

#surface3d(surf$x, surf$y, surf$temperatura)

