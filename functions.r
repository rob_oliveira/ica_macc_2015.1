
norm_vector <- function(v){
	sqrt((v%*%v)[1,1])
}

permuteRows <- function(data_){
	nr = dim(data_)[1]
	permute = data[sample.int(nr),]

	return(permute)
}

	
normResultados <- function(resultados_){
	for (i in 1:nrow(resultados_)) {
		if(resultados_[i,1] == 0){
			resultados_[i, 1] = -1
		}
	}
	return(resultados_)
}



normDados <- function(valores_){
	media = rep(0, ncol(valores_))

	for(i in 1:nrow(valores_)){
		media = media + valores_[i,]
	}

	media = media/nrow(valores_-1)

	desvio_padrao = rep(0, ncol(valores_))

	for(i in 1:nrow(valores_)){
		desvio_padrao = desvio_padrao + (valores_[i,] - media)^2
	}
	desvio_padrao = desvio_padrao/(nrow(valores_)-1)

	for (i in 1:nrow(valores_)) {
		valores_[i, ] <- as.matrix((valores_[i, ] - media)/desvio_padrao)
	}
	#nr = dim(valores_)[1]
	##valores_ = valores_[sample.int(nr),]
	return(valores_)
}


k_means <- function(k, valores_treinamento){

	centers = matrix(ncol=ncol(valores_treinamento), nrow=k)

	for(i in 1:k){
		centers[i, ] = valores_treinamento[i, ]
	}
	centers_old = centers
	cluster_count = rep(0,k)
	variancia = rep(0, k)
	diff = 1
	while(diff > 1e-4){ #2nn 
		centers_old = centers
		clusters = matrix(0, ncol=ncol(valores_treinamento), nrow=k)
		cluster_count = rep(1,k)
		variancia = rep(0, k)
		for (i in 1:nrow(valores_treinamento)) {
			min = 1
			distance_old = norm_vector(valores_treinamento[i, ] - centers[1,])
			
			for (j in 1:k) {
				distance =  norm_vector(valores_treinamento[i, ] - centers[j,])
				if(distance < distance_old){
					min = j
				}
				distance_old = distance
			}
			
			clusters[min,] = valores_treinamento[i, ] + clusters[min,]
			cluster_count[min] = cluster_count[min] +1
			
		}
		for (j in 1:k) {
			centers[j,] = clusters[j,]/cluster_count[j]
			diff = diff + norm_vector(centers[j, ] - centers_old[j,])
		}
		
		diff = diff/nrow(centers)
	}
	return(centers)
}

k_variancia <-function(k, centros, valores_treinamento){
	variancia = rep(0, k)
	cluster_count = rep(1,k)
	for (i in 1:nrow(valores_treinamento)) {
		min = 1
		distance_old = norm_vector(valores_treinamento[i, ] - centros[1,])
		for (j in 1:k) {
			distance =  norm_vector(valores_treinamento[i, ] - centros[j,])
			if(distance < distance_old){
				min = j
			}
			distance_old = distance
		}
		
		variancia[min] = variancia[min] + norm_vector(valores_treinamento[i, ] - centros[k,])
		cluster_count[min] = cluster_count[min] +1
	}

	return(variancia/cluster_count)
}

getConfusion <- function(resultado_, erro_){
	confusao = matrix( c(0,0,0,0), nrow=2, ncol=2, byrow=TRUE)
	if(resultado_ > 0 ) {
			if(erro_==0){ confusao[1,1] = confusao[1,1]+1}
			else{ confusao[1, 2] = confusao[1, 2]+1}
	}
	else{
		if(erro_==0) confusao[2,2] = confusao[2,2]+1
		else confusao[2, 1] = confusao[2, 1]+1
	}
	
	return(confusao)
}

getAccuracy <- function(confusao) {
	tp = confusao[1,1]
	tn = confusao[2,2]
	total = tp + tn + confusao[1,2] + confusao[2,1]
	return( (tp+tn)/total)
}

getSensibility <- function(confusao) {
	truePositive = confusao[1,1]
	totalPositive= confusao[1,1] + confusao[2,1]
	return( (truePositive/totalPositive))
}

geteSpecificity <- function(confusao) {
	truenNegative = confusao[2,2]
	totalNegative= confusao[1,2] + confusao[2,2]
	return( (truenNegative/totalNegative))
}
