
getDataSet <- function(){

	#data = read.csv("datasets/iris.csv",  header=FALSE)
	#data = read.csv("datasets/wine.csv",  header=FALSE)
	#data = read.csv("datasets/dermatology.csv",  header=FALSE) , nrows=100000
	data = read.table("datasets/data.txt",  header=FALSE, fill=TRUE)
	position = read.table("datasets/positions.txt",  header=FALSE, fill=TRUE)
	day_subset = subset(data, V1 ==  "2004-02-29")
	hora_1 = "06:00:00"
	hora_1_diff = "06:05:00"
	hora_2 = "12:00:00"
	hora_2_diff = "12:05:00"
	hora_3 = "18:00:00"
	hora_3_diff = "18:05:00"
	hora_4 = "00:00:00"
	hora_4_diff = "00:05:00" 

	timerow = strptime(day_subset$V2,"%H:%M:%S")

	time_1 = strptime(hora_1,"%H:%M:%S")
	time_1_diff = strptime(hora_1_diff,"%H:%M:%S")
	time_2 = strptime(hora_2,"%H:%M:%S")
	time_2_diff = strptime(hora_2_diff,"%H:%M:%S")
	time_3 = strptime(hora_3,"%H:%M:%S")
	time_3_diff = strptime(hora_3_diff,"%H:%M:%S")
	time_4 = strptime(hora_4,"%H:%M:%S")
	time_4_diff = strptime(hora_4_diff,"%H:%M:%S")

	set1 <- matrix(,,ncol(day_subset))
	set2 <- matrix(,,ncol(day_subset))
	set3 <- matrix(,,ncol(day_subset))
	set4 <- matrix(,,ncol(day_subset))

	for (i in 1:nrow(day_subset)) {
		#timerow = strptime(day_subset[i,]$V2,"%H:%M:%S")
		if(time_1 < timerow[i] && time_1_diff > timerow[i]){
				set1 = rbind(set1, day_subset[i,])
		}

		else if(time_2 < timerow[i] && time_2_diff > timerow[i]){
				set2 = rbind(set2, day_subset[i,])
		}

		else if(time_3 < timerow[i] && time_3_diff > timerow[i]){
				set3 = rbind(set3, day_subset[i,])
		}

		else if(time_4 < timerow[i] && time_4_diff > timerow[i]){
				set4 = rbind(set4, day_subset[i,])
		}
		
		print(i)
	}
##########################################
	hora_posicao1 <- matrix(nrow=1, ncol=7)
	hora_posicao2 <- matrix(nrow=1, ncol=7)
	hora_posicao3 <- matrix(nrow=1, ncol=7)
	hora_posicao4 <- matrix(nrow=1, ncol=7)
	for (i in 1:nrow(position)) {
		for (j in 2:nrow(set1)) {

			if(!is.na(set1[j,4]) && i == set1[j,4]){
				tmp = merge(position[i,], set1[j,c(5:8)])
				#names(hora_posicao1) <- names(tmp)
				hora_posicao1 = rbind(hora_posicao1, as.matrix(tmp))
			}
		}
		for (j in 2:nrow(set2)) {
			if(!is.na(set2[j,4]) && i == set2[j,4]){
				tmp = merge(position[i,], set2[j,c(5:8)])
				#names(hora_posicao2) <- names(tmp)
				hora_posicao2 = rbind(hora_posicao2, as.matrix(tmp))
			}
		}
		for (j in 2:nrow(set3)) {
			if(!is.na(set3[j,4]) && i == set3[j,4]){
				tmp = merge(position[i,], set3[j,c(5:8)])
				#names(hora_posicao3) <- names(tmp)
				hora_posicao3 = rbind(hora_posicao3, as.matrix(tmp))
			}
		}
		for (j in 2:nrow(set4)) {
			if(!is.na(set4[j,4]) && i == set4[j,4]){
				tmp = merge(position[i,], set4[j,c(5:8)])
				#names(hora_posicao4) <- names(tmp)
				hora_posicao4 = rbind(hora_posicao4, as.matrix(tmp))
			}
		}

	}
	write.table(hora_posicao1[-1,], file="teste_hora_1", col.names = F, row.names = F)
	write.table(hora_posicao2[-1,], file="teste_hora_2", col.names = F, row.names = F)
	write.table(hora_posicao3[-1,], file="teste_hora_3", col.names = F, row.names = F)
	write.table(hora_posicao4[-1,], file="teste_hora_4", col.names = F, row.names = F)
}


getDataSet()